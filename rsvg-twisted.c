/*
 * This code is copied from cairotwisted.c in Pango examples:
 *
 * https://github.com/phuang/pango/blob/master/examples/cairotwisted.c
 *
 * Written by Behdad Esfahbod, 2006..2007
 * Copied to librsvg by Mika Heiskanen, 2014
 *
 * Permission to use, copy, modify, distribute, and sell this example
 * for any purpose is hereby granted without fee.
 * It is provided "as is" without express or implied warranty.
 */

#include <math.h>
#include <stdlib.h>
#include <pango/pangocairo.h>

#define MAX_SEGMENTS 4

/* Returns Euclidean distance between two points */
static double
chord_length (double x1, double y1, double x2, double y2)
{
  double dx, dy;
  dx = x2-x1;
  dy = y2-y1;
  return sqrt (dx * dx + dy * dy);
}

/* Returns length of a Bezier curve.
 * Seems like computing that analytically is not easy.  The
 * code just flattens the curve using cairo and adds the length
 * of segments.
 */

static double
cubic_length(double x1, double y1,
	     double x2, double y2,
	     double x3, double y3,
	     double x4, double y4)
{
  double eps, chord, arc, l1, l2;
  double xb1, xb2, xb3, xc1, xc2, xd1;
  double yb1, yb2, yb3, yc1, yc2, yd1;

  chord = chord_length(x1,y1,x4,y4);

  if(chord == 0)
    return 0;

  arc = (chord_length(x1,y1,x2,y2) +
	 chord_length(x2,y2,x3,y3) +
	 chord_length(x3,y3,x4,y4));

  /* If relative difference is small, the mean of the values
   * is a good estimate.
   */

  eps = 0.01;

  if( (arc-chord)/arc < eps)
    return 0.5*(arc+chord);

  xb1 = (x1+x2)/2;   yb1 = (y1+y2)/2;
  xb2 = (x2+x3)/2;   yb2 = (y2+y3)/2;
  xb3 = (x3+x4)/2;   yb3 = (y3+y4)/2;
  xc1 = (xb1+xb2)/2; yc1 = (yb1+yb2)/2;
  xc2 = (xb2+xb3)/2; yc2 = (yb2+yb3)/2;
  xd1 = (xc1+xc2)/2; yd1 = (yc1+yc2)/2;

  l1 = cubic_length(x1,y1, xb1,yb1, xc1,yc1, xd1,yd1);
  l2 = cubic_length(xd1,yd1, xc2,yc2, xb3,yb3, x4,y4);

  return l1+l2;
}

typedef double parametrization_t;

/* Compute parametrization info.  That is, for each part of the 
 * cairo path, tags it with its length.
 *
 * Free returned value with g_free().
 */
parametrization_t *
parametrize_path (cairo_path_t *path)
{
  int i;
  cairo_path_data_t *data, last_move_to, current_point;
  parametrization_t *parametrization;

  parametrization = g_malloc (path->num_data * sizeof (parametrization[0]));

  for (i=0; i < path->num_data; i += path->data[i].header.length) {
      data = &path->data[i];
      parametrization[i] = 0.0;
      switch (data->header.type) {
      case CAIRO_PATH_MOVE_TO:
          last_move_to = data[1];
          current_point = data[1];
          break;
      case CAIRO_PATH_CLOSE_PATH:
          /* Make it look like it's a line_to to last_move_to */
          data = (&last_move_to) - 1;
          /* fall through */
      case CAIRO_PATH_LINE_TO:
	parametrization[i] = chord_length (current_point.point.x,
					   current_point.point.y,
					   data[1].point.x,
					   data[1].point.y);
	current_point = data[1];
	break;
      case CAIRO_PATH_CURVE_TO:
          parametrization[i] = cubic_length (current_point.point.x, current_point.point.y,
                                             data[1].point.x, data[1].point.y,
                                             data[2].point.x, data[2].point.y,
                                             data[3].point.x, data[3].point.y);
          
          current_point = data[3];
          break;
      default:
          g_assert_not_reached ();
      }
	  printf ("%d %.2f", i, parametrization[i]);
  }
  
  return parametrization;
}

/* Compute path length */

double path_length (const cairo_path_t * path)
{
  double length;
  int i;
  cairo_path_data_t *data, last_move_to, current_point;

  if(!path)
    return 0;

  length = 0;
  for (i=0; i < path->num_data; i += path->data[i].header.length) {
      data = &path->data[i];
      switch (data->header.type) {
      case CAIRO_PATH_MOVE_TO:
          last_move_to = data[1];
          current_point = data[1];
          break;
      case CAIRO_PATH_CLOSE_PATH:
          /* Make it look like it's a line_to to last_move_to */
          data = (&last_move_to) - 1;
          /* fall through */
      case CAIRO_PATH_LINE_TO:
	length += chord_length (current_point.point.x,
				current_point.point.y,
				data[1].point.x,
				data[1].point.y);
          current_point = data[1];
          break;
      case CAIRO_PATH_CURVE_TO:
          /* naive curve-length, treating bezier as three line segments:
             parametrization[i] = two_points_distance (&current_point, &data[1])
             + two_points_distance (&data[1], &data[2])
             + two_points_distance (&data[2], &data[3]);
          */
	  length += cubic_length (current_point.point.x, current_point.point.y,
				  data[1].point.x, data[1].point.y,
				  data[2].point.x, data[2].point.y,
				  data[3].point.x, data[3].point.y);
          current_point = data[3];
          break;
      default:
          g_assert_not_reached ();
      }
  }
  
  return length;
}


typedef void (*transform_point_func_t) (void *closure, double *x, double *y);

/* Project a path using a function.  Each point of the path (including
 * Bezier control points) is passed to the function for transformation.
 */
static void
transform_path (cairo_path_t *path, transform_point_func_t f, void *closure)
{
  int i;
  cairo_path_data_t *data;

  for (i=0; i < path->num_data; i += path->data[i].header.length) {
    data = &path->data[i];
    switch (data->header.type) {
    case CAIRO_PATH_CURVE_TO:
      f (closure, &data[3].point.x, &data[3].point.y);
      f (closure, &data[2].point.x, &data[2].point.y);
    case CAIRO_PATH_MOVE_TO:
    case CAIRO_PATH_LINE_TO:
      f (closure, &data[1].point.x, &data[1].point.y);
      break;
    case CAIRO_PATH_CLOSE_PATH:
      break;
    default:
	g_assert_not_reached ();
    }
  }
}


/* Simple struct to hold a path and its parametrization */
typedef struct {
  cairo_path_t *path;
  parametrization_t *parametrization;
} parametrized_path_t;


/* Project a point X,Y onto a parameterized path.  The final point is
 * where you get if you walk on the path forward from the beginning for X
 * units, then stop there and walk another Y units perpendicular to the
 * path at that point.  In more detail:
 *
 * There's three pieces of math involved:
 *
 *   - The parametric form of the Line equation
 *     http://en.wikipedia.org/wiki/Line
 *
 *   - The parametric form of the Cubic Bézier curve equation
 *     http://en.wikipedia.org/wiki/B%C3%A9zier_curve
 *
 *   - The Gradient (aka multi-dimensional derivative) of the above
 *     http://en.wikipedia.org/wiki/Gradient
 *
 * The parametric forms are used to answer the question of "where will I be
 * if I walk a distance of X on this path".  The Gradient is used to answer
 * the question of "where will I be if then I stop, rotate left for 90
 * degrees and walk straight for a distance of Y".
 */
void
point_on_path (parametrized_path_t *param,
	       double *x, double *y)
{
  int i;
  double ratio, the_y = *y, the_x = *x, dx, dy;
  cairo_path_data_t *data, last_move_to, current_point;
  cairo_path_t *path = param->path;
  parametrization_t *parametrization = param->parametrization;

  printf("point_on_path: ");
  for (i=0; i + path->data[i].header.length < path->num_data &&
	    (the_x > parametrization[i] ||
	     path->data[i].header.type == CAIRO_PATH_MOVE_TO);
       i += path->data[i].header.length) {
	printf("%.2f ", the_x);
    the_x -= parametrization[i];
    data = &path->data[i];
    switch (data->header.type) {
    case CAIRO_PATH_MOVE_TO:
	current_point = data[1];
        last_move_to = data[1];
	break;
    case CAIRO_PATH_LINE_TO:
	current_point = data[1];
	break;
    case CAIRO_PATH_CURVE_TO:
	current_point = data[3];
	break;
    case CAIRO_PATH_CLOSE_PATH:
	break;
    default:
	g_assert_not_reached ();
    }
  }
  printf("\n");
  data = &path->data[i];

  switch (data->header.type) {

  case CAIRO_PATH_MOVE_TO:
      break;
  case CAIRO_PATH_CLOSE_PATH:
      /* Make it look like it's a line_to to last_move_to */
      data = (&last_move_to) - 1;
      /* fall through */
  case CAIRO_PATH_LINE_TO:
      {
	ratio = the_x / parametrization[i];
	/* Line polynomial */
	*x = current_point.point.x * (1 - ratio) + data[1].point.x * ratio;
	*y = current_point.point.y * (1 - ratio) + data[1].point.y * ratio;

	/* Line gradient */
	dx = -(current_point.point.x - data[1].point.x);
	dy = -(current_point.point.y - data[1].point.y);

	/*optimization for: ratio = the_y / sqrt (dx * dx + dy * dy);*/
	ratio = the_y / parametrization[i];
	*x += -dy * ratio;
	*y +=  dx * ratio;
      }
      break;
  case CAIRO_PATH_CURVE_TO:
      {
	/* FIXME the formulas here are not exactly what we want, because the
	 * Bezier parametrization is not uniform.  But I don't know how to do
	 * better.  The caller can do slightly better though, by flattening the
	 * Bezier and avoiding this branch completely.  That has its own cost
	 * though, as large y values magnify the flattening error drastically.
	 */

        double ratio_1_0, ratio_0_1;
	double ratio_2_0, ratio_0_2;
	double ratio_3_0, ratio_2_1, ratio_1_2, ratio_0_3;
	double _1__4ratio_1_0_3ratio_2_0, _2ratio_1_0_3ratio_2_0;

	ratio = the_x / parametrization[i];

	ratio_1_0 = ratio;
	ratio_0_1 = 1 - ratio;

	ratio_2_0 = ratio_1_0 * ratio_1_0; /*      ratio  *      ratio  */
	ratio_0_2 = ratio_0_1 * ratio_0_1; /* (1 - ratio) * (1 - ratio) */

	ratio_3_0 = ratio_2_0 * ratio_1_0; /*      ratio  *      ratio  *      ratio  */
	ratio_2_1 = ratio_2_0 * ratio_0_1; /*      ratio  *      ratio  * (1 - ratio) */
	ratio_1_2 = ratio_1_0 * ratio_0_2; /*      ratio  * (1 - ratio) * (1 - ratio) */
	ratio_0_3 = ratio_0_1 * ratio_0_2; /* (1 - ratio) * (1 - ratio) * (1 - ratio) */

	_1__4ratio_1_0_3ratio_2_0 = 1 - 4 * ratio_1_0 + 3 * ratio_2_0;
	_2ratio_1_0_3ratio_2_0    =     2 * ratio_1_0 - 3 * ratio_2_0;

	/* Bezier polynomial */
	*x = current_point.point.x * ratio_0_3
	   + 3 *   data[1].point.x * ratio_1_2
	   + 3 *   data[2].point.x * ratio_2_1
	   +       data[3].point.x * ratio_3_0;
	*y = current_point.point.y * ratio_0_3
	   + 3 *   data[1].point.y * ratio_1_2
	   + 3 *   data[2].point.y * ratio_2_1
	   +       data[3].point.y * ratio_3_0;

	/* Bezier gradient */
	dx =-3 * current_point.point.x * ratio_0_2
	   + 3 *       data[1].point.x * _1__4ratio_1_0_3ratio_2_0
	   + 3 *       data[2].point.x * _2ratio_1_0_3ratio_2_0
	   + 3 *       data[3].point.x * ratio_2_0;
	dy =-3 * current_point.point.y * ratio_0_2
	   + 3 *       data[1].point.y * _1__4ratio_1_0_3ratio_2_0
	   + 3 *       data[2].point.y * _2ratio_1_0_3ratio_2_0
	   + 3 *       data[3].point.y * ratio_2_0;

	ratio = the_y / sqrt (dx * dx + dy * dy);
	*x += -dy * ratio;
	*y +=  dx * ratio;
      }
      break;
  default:
      g_assert_not_reached ();
  }
}

/* Projects the current path of cr onto the provided path. */
void
map_path_onto (cairo_t *cr, cairo_path_t *path)
{
  cairo_path_t *current_path;
  parametrized_path_t param;

  param.path = path;
  param.parametrization = parametrize_path (path);

  current_path = cairo_copy_path (cr);
  cairo_new_path (cr);

  transform_path (current_path,
		  (transform_point_func_t) point_on_path, &param);

  cairo_append_path (cr, current_path);

  cairo_path_destroy (current_path);
  g_free (param.parametrization);
}

/* For a y-value, find a run (an x1-x2 range) */
void
path_run (cairo_path_t *path,
		  double y, double (*runs)[2], int* n)
{
  double xx1, xx2, dx, dy, ratio, x1, x2, y1, y2, save;
  int i, j = *n;
  gboolean set1, set2, setd, clockwise, set_save;
  cairo_path_data_t *data, last_move_to, current_point, prev_point;

  if(!path)
    return;

  xx1 = xx2 = 0;
  set1 = set2 = setd = clockwise = set_save = FALSE;
  printf ("h = %f\n", y);
  // Last data point is close path
  for (i = 0; i < path->num_data; i += path->data[i].header.length) {
      data = &path->data[i];
      printf ("%d %d %d\n", i, data->header.type, data->header.length);

      switch (data->header.type) {
      case CAIRO_PATH_MOVE_TO:
		last_move_to = data[1];
		current_point = data[1];
		break;
      case CAIRO_PATH_CLOSE_PATH:
		/* Make it look like it's a line_to to last_move_to */
		data = (&last_move_to) - 1;
		/* fall through */
		/* break; */
      case CAIRO_PATH_LINE_TO:
		current_point = data[1];
		break;
      case CAIRO_PATH_CURVE_TO:
		current_point = data[3];
		break;
      default:
		g_assert_not_reached ();
      }

	  if (data[1].point.y > INT_MAX) exit(1);
	  if (i > 0) {
		x1 = prev_point.point.x;
		y1 = prev_point.point.y;
		x2 = current_point.point.x;
		y2 = current_point.point.y;
		dy = y2 - y1;
		dx = x2 - x1;
		ratio = (abs (dy) > 0) ? dx / dy : 0;
		if ((dy > 0) && (y1 <= y) && (y2 > y)) {
		  xx1 = x1 + ratio * (y - y1);
		  set1 = TRUE;
		}
		if ((dy < 0) && (y1 > y) && (y2 <= y)) {
		  xx2 = x2 + ratio * (y - y2);
		  set2 = TRUE;
		}
		if (!setd) {
		  clockwise = !((dy <= 0 && dx <= 0) || (dy >= 0 && dx >= 0));
		  setd = TRUE;
		}
		if (j == 0 && !set_save && !set2 && set1) {
		  if (clockwise) 
			runs[0][1] = xx1;
		  else
			runs[0][0] = xx1;
		  set_save = TRUE;
		  set1 = FALSE;
		  j++;
		}
		printf ("x %d %d   %d   %.2f %.2f\n", set_save, set1, set2, x1, x2);
		printf ("y %d %.2f %.2f %.2f %.2f\n", clockwise, xx1, xx2, y1, y2);
		if (set1 && set2 && j < MAX_SEGMENTS) {
		  if (clockwise) {
			runs[j][0] = xx2;
			runs[j++][1] = xx1;
		  } else {
			runs[j][0] = xx1;
			runs[j++][1] = xx2;
		  }
		  xx1 = xx2 = 0;
		  set1 = set2 = FALSE;
		}
	  }
	  prev_point = current_point;
  }
  if (set_save) {
	if (set2) {
	  if (clockwise)
		runs[0][0] = xx2;
	  else
		runs[0][1] = xx2;
	} else {
	  if (clockwise)	  
		runs[0][1] = 0;
	  else
		runs[0][0] = 0;
	}
  }
  
  *n = j;
}

/* A shape is a collection of curves. */
/* A curve which adds a region to the shape is called an inclusion shape. */
/* A curve which removes a region from the shape is called an exclusion shape. */
double**
shape_run (cairo_path_t **shapes, int *m, double y)
{
  int i, n = 1;
  /* Allowing 4 runs per curve (M shape) */
  double (*runs)[2] = malloc (sizeof (double[MAX_SEGMENTS][2]));

  i = *m = 0;
  for (i = 0; i < n; i++) {
	path_run (shapes[i], y, runs, m);
	/* Handle inclusion/exclusion */
  }
  for (i = 0; i < *m; i++) {
	printf("\nrun %d: %.2f %.2f\n", i, runs[i][0], runs[i][1]);
  }
  /* free (runs); */
  return runs;
}
