/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/* vim: set sw=4 sts=4 ts=4 expandtab: */
/*
  rsvg-twisted.h

*/

#ifndef RSVG_TWISTED_H
#define RSVG_TWISTED_H

#include "rsvg-private.h"

G_BEGIN_DECLS

G_GNUC_INTERNAL
void map_path_onto (cairo_t *cr, cairo_path_t *path);
double path_length (const cairo_path_t * path);
double** shape_run (cairo_path_t **shapes, int *n, double y);


G_END_DECLS

#endif /* RSVG_TWISTED_H */

